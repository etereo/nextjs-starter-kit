import { addDecorator, addParameters, configure } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
import { MuiThemeProvider } from '@material-ui/core'

import theme from '../src/modules/theme'
import { GlobalStyles } from '../src/modules/ui/components'

addDecorator(withKnobs)

const withTheme = (storyFn) => (
  <>
    <GlobalStyles />
    <MuiThemeProvider theme={theme}>{storyFn()}</MuiThemeProvider>
  </>
)
addDecorator(withTheme)

addParameters({
  options: {
    theme: theme.light,
  },
  viewport: {
    viewports: {
      smallMobile: {
        name: 'small mobile | 320x568 | iPhoneSE',
        styles: {
          width: '320px',
          height: '568px',
        },
      },
      mobile: {
        name: 'mobile | 375x812 | iPhoneX',
        styles: {
          width: '375px',
          height: '812px',
        },
      },
      phablet: {
        name: 'phablet | 411x823 | Pixel 2 XL',
        styles: {
          width: '411px',
          height: '823px',
        },
      },
      tablet: {
        name: 'tablet | 768x1024 | iPad',
        styles: {
          width: '768px',
          height: '1024px',
        },
      },
      desktop: {
        name: 'desktop | 1250x1024',
        styles: {
          width: '1250px',
          height: '1024px',
        },
      },
      bigDesktop: {
        name: 'bigDesktop | 3840x2160 | 4k',
        styles: {
          width: '3840px',
          height: '2160px',
        },
      },
    },
  },
})

// automatically import all files ending in *.stories.jsx
const req = require.context('../src', true, /\.stories\.tsx$/)
function loadStories() {
  req.keys().forEach((filename) => req(filename))
}

configure(loadStories, module)
