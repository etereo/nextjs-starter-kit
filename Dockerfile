FROM node:12.16-buster

# Environment
ENV HOME=/home/app/
ENV NODE_ENV=production

WORKDIR $HOME

# Source and dependencies manifests
COPY . $HOME

# Dynamic ENV params
ARG PACKAGE
ENV PACKAGE=$PACKAGE

ARG APP_CONFIG_ENV=development
ENV APP_CONFIG_ENV=$APP_CONFIG_ENV

RUN echo APP_CONFIG_ENV=$APP_CONFIG_ENV
RUN echo PACKAGE=$PACKAGE

WORKDIR $HOME/pkg/${PACKAGE}

RUN npm install
RUN npm run build

EXPOSE 3000

CMD ["npm", "start"]
