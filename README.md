# nextjs-starter-kit

NextJS starter project

## You will get

* NextJS + TS
* jest
* Storybook
* MaterialUI
* Sentry
* Linters
* Dockerized app
* redux
* redux-observable
* Typesafe redux
* Webpack bundle analyzer

## You could apply best practices like:

* Lint+format code before commit
* Semantic version + generated changelog
* Dynamic SEO Metadata per page
* deps + Artifact cache for CI
* Makefile commands
* Runtime/Buildtime config
* Typed redux
