JEST_OPTIONS?=--changedSince refs/heads/master
# BRANCH_NAME?=$(shell git branch --show-current) # requires git v2.2x.x
BRANCH_NAME?=$(shell git rev-parse --abbrev-ref HEAD)

# master branch will compare with it's previous commit
# other branches will compare with master HEAD
.PHONY: init-script
init-script:
ifeq ($(BRANCH_NAME),master)
	$(eval JEST_OPTIONS=--changedSince refs/heads/master~1)
endif

# cleans all installed node_modules
.PHONY: clean
clean:
	npm run clean
	# npm cache clear --force

# install dependencies
.PHONY: install
install:
	# npm ci --ignore-scripts --production --no-optional
	npm install

# re-install dependencies
# .PHONY: reinstall
# reinstall:
# 	npm run clean
# 	npm run clean:lock
# 	npm install

# lint all your code
.PHONY: lint
lint:
	npm run lint

.PHONY: run
run:
	npm run dev

.PHONY: test
test: init-script
	npm run test -- ${JEST_OPTIONS} ${ARGS}

.PHONY: build
build:
	npm run build

.PHONY: analyze
analyze:
	ANALYZE=true npm run build

.PHONY: build-docker
build-docker:
	npm run build:docker

.PHONY: run-docker
run-docker:
	npm run start:docker

.PHONY: release
release:
	npx standard-version

.PHONY: ui
ui:
	npm run storybook
