const withPlugins = require('next-compose-plugins')
const withImages = require('next-images')
const withTranspileModules = require('next-transpile-modules')
const withSourceMaps = require('@zeit/next-source-maps')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: !!process.env.ANALYZE,
})
const envConfig = require('./src/config')

module.exports = withPlugins(
  [withTranspileModules(['@masfront']), withImages, withSourceMaps, withBundleAnalyzer],
  {
    typescript: {
      ignoreDevErrors: false,
      ignoreBuildErrors: false,
    },
    webpack: (config, { isServer }) => {
      // eslint-disable-next-line no-param-reassign
      const imgRule = config.module.rules.find((rule) => {
        return (rule.test ? rule.test.toString() : '').indexOf('svg') !== -1
      })
      if (imgRule) {
        imgRule.test = /\.(jpe?g|png|gif|ico|webp)$/
      }

      config.module.rules.push({
        test: /\.svg$/,
        use: ['@svgr/webpack', 'url-loader'],
      })

      if (!isServer) {
        // eslint-disable-next-line no-param-reassign
        config.resolve.alias['@sentry/node'] = '@sentry/browser'
      }

      return config
    },
    publicRuntimeConfig: {
      config: envConfig,
    },
    serverRuntimeConfig: {
      SENTRY_TOKEN: 'SENTRY_TOKEN',
    },
    exportTrailingSlash: false,
  },
)
