import { server } from './src/modules/server'

server().then((app) => {
  console.info('running', !!app)
})
