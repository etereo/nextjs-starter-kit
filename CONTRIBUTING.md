# Git Commit Guidelines
We have very precise rules over how our git commit messages can be formatted. This leads to more readable messages that are easy to follow when looking through the project history. But also, we use the git commit messages to generate the change log **(TO DO)**.

## Commit Message Format
Based on this old article https://bit.ly/2npjWQw and

angular commit conventions https://bit.ly/2O08O7l

Each commit message consists of a **header**, and a **body**.

The header is mandatory. It has a special format that includes a **type**, a **scope**, a **subject** and an optional **issue_id**:
```
<type>(<scope>): <subject> (<issue_id>)
<BLANK LINE>
<body>
```
For example:
- **feat(cancelOrder): TMPV-666 add custom behavior to combo**
- **fix(sidebar): XXX-667 consume data from different selector**

**Allowed `<type>:`**

- feature
- hotfix (bug fix with the environment)
- fix (bug fix)
- docs (documentation)
- style (formatting, missing semicolons, …)
- refactor
- test (when adding missing tests)
- chore (maintain)

**Next branch types will generate a k8s_deployment on CI:**

- feature
- hotfix
- feat
- userstory
- ft
- us
- hf

###### _Any line of the commit message cannot be longer 50 characters. This allows the message to be easier to read in various git tools._

## Subject

A short phrase explaining the commit changes, should be capitalized and using imperative present tense.

## Body

Just as in the subject, use the imperative, present tense. The body should include the motivation for the change and contrast this with previous behavior. It's an _optional_ deeper description.

## Using commitizen
We use commitizen cli https://github.com/commitizen/cz-cli to create new commits.
When you commit with Commitizen, you'll be prompted to fill out any required commit fields at commit time.
To install commitizen in the project, do:

```
npm install commitizen -g

commitizen init cz-conventional-changelog --save-dev --save-exact
```

To create new commits through commitizen do:

`git add .` (add the changes)
`git cz` (create the commit with commitizen)

## Pushing a change

Before you submit your changes consider the following:
* Search for an open or closed Issue that relates to your stuff. You don't want to duplicate effort.
* Develop your stuff in a new branch
* Build your changes locally to ensure all tests pass and everything works as expected
* If everything works as expected, **and your branch is in sync with master** then open a **Pull Request**
* If we suggest changes then:
** Make the required updates
** Rebuild to ensure tests are still passing and everything still works
** Push your branch to the remote repository
** If needed, sync your branch with master (we recommend using merge instead of rebase)

### Opening a Pull Request

You can open a PR against any branch.

Master branch is the standard branch to work with, where feature branches will be merged.

Use following  **naming for** PR titles: <type>(<section>): <ISSUE-ID> <SUBJECT>

#### Example:

**feat(checkout-v2): TMPV-1234 my awesome stuff**

Inspired by [https://gist.github.com/stephenparish/9941e89d80e2bc58a153](https://gist.github.com/stephenparish/9941e89d80e2bc58a153)

**Allowed `<type>:`**

- feat (feature)
- hotfix
- fix (bug fix)
- docs (documentation)
- style (formatting, missing semicolons, …)
- refactor
- test (when adding missing tests)
- chore (maintain)

**Examples for <section>:**

- home
- profile
- dashboard
- detail-view
- ...
  

## Merging a Pull Request

Once a PR has been reviewed by dev team and has e2e and has been tested by qa/product in a feature branch (must be synced with master), the PR can be merged.

### Tips for merging a PR:
- Ensure that commits compile to these conventions, or at least one (in that case squash all commits except the one that compiles)
- Ensure you **remove the branch** after merging the PR
