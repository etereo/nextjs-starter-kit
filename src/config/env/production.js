module.exports = {
  ssrSharedCache: false,
  debug: false,
  sentryDsn: 'https://xxxx@sentry.com/XXX',
  APIBaseUrl: 'https://url.base.com',
  GTMCode: 'GTM-XXXXXX',
  redisUrl: 'redis://redis:6379/2',
}
