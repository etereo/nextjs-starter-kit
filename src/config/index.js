const environtments = ['local', 'development', 'staging', 'production']

const env =
  environtments.find((environment) => environment.indexOf(process.env.APP_CONFIG_ENV) !== -1) ||
  'development'

// eslint-disable-next-line import/no-dynamic-require
const config = require(`./env/${env}`)
config.name = process.env.npm_package_name
config.version = process.env.npm_package_version
config.environment = env

module.exports = config
