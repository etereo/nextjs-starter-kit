import next from 'next'
import helmet from 'helmet'
import compression from 'compression'
import express from 'express'
import { sourcemapsForSentryOnlyHandler, sentryInit } from '../sentry'
import { trailingSlashUrlHandler } from './server.helpers'

const port = parseInt(process.env.PORT || '3000', 10)
const dev = 'production'.indexOf(process.env.NODE_ENV || 'development') === -1
const app = next({ dev })
const defaultNextHandler = app.getRequestHandler()
const server = express()

console.info('NODE_ENV', process.env.NODE_ENV)
console.info('APP_CONFIG_ENV', process.env.APP_CONFIG_ENV)

const {
  publicRuntimeConfig: { config },
  serverRuntimeConfig: { SENTRY_TOKEN },
} = app.nextConfig

const { Sentry } = sentryInit({
  dsn: config.sentryDsn,
  environment: config.environment,
  release: `${config.name}@${config.version}`,
})

export default async function serve() {
  await app.prepare()

  server.use(
    helmet({
      noSniff: false,
    }),
  )

  // no cache by default
  server.set('etag', false)
  server.use((req, res, nextcb) => {
    res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private')
    nextcb()
  })

  server.use(
    compression({
      level: 9,
      threshold: '1kb',
      filter: function shouldCompress(req, res) {
        const reg = /MSIE [1-6]\./g
        return !reg.test(req.headers['user-agent'] || '')
      },
    }),
  )

  // Static cache
  server.use(
    '/static',
    express.static('public/static', {
      index: false,
      maxAge: 86400000 * 30, // 30 days
      etag: false,
    }),
  )

  server.use(
    '/_next',
    express.static('.next', {
      index: false,
      maxAge: 86400000 * 30, // 30 days
      etag: false,
    }),
  )

  // Monitoring Heath
  server.get('/health', (_, res: express.Response) => {
    res.status(200)
    res.end()
  })

  // This attaches request information to Sentry errors
  server.use(Sentry.Handlers.requestHandler())
  server.get(/\.map$/, sourcemapsForSentryOnlyHandler(SENTRY_TOKEN, config.environment))
  // 301 Trailing slash Redirects
  server.use(trailingSlashUrlHandler)

  // Ssr Handler
  server.get('*', (req: express.Request, res: express.Response) => defaultNextHandler(req, res))

  // This handles errors if they are thrown before reaching the app
  server.use(Sentry.Handlers.errorHandler())

  return server.listen(port, (err) => {
    if (err) {
      throw err
    }
    // eslint-disable-next-line no-console
    console.log(`> Ready on http://localhost:${port}`)
  })
}
