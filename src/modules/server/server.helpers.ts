/* eslint-disable import/prefer-default-export */
import express from 'express'

export function trailingSlashUrlHandler(
  req: express.Request,
  res: express.Response,
  nextcb: express.NextFunction,
) {
  if (req.path.substr(-1) === '/' && req.path.length > 1) {
    let trailingSlashUrl = req.path.substr(0, req.path.length - 1)
    if (req.url.indexOf('?') !== -1) {
      trailingSlashUrl += req.url.substr(req.url.indexOf('?'))
    }
    res.redirect(301, trailingSlashUrl)
  } else {
    nextcb()
  }
}
