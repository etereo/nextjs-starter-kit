import React from 'react'
import { MuiThemeProvider, Theme } from '@material-ui/core/styles'

interface AppShellProps {
  children: React.ReactNode
  theme: Theme
}

const AppShell: React.FunctionComponent<AppShellProps> = ({ children, theme }) => {
  return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
}

export default AppShell
