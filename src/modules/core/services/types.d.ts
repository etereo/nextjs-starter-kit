import {} from 'typesafe-actions'

// example type-safe services typings
declare module 'typesafe-actions' {
  export type RootService = typeof import('./index').default
}
