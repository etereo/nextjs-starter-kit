import samplesService from '../../samples/services/samples.service'

const rootService = {
  api: {
    samples: samplesService,
  },
}

export type RootService = typeof rootService

export default rootService
