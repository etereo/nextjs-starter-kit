import { combineEpics } from 'redux-observable'
import samplesEpics from '../../samples/store/samples.epics'

const rootEpic = combineEpics(...Object.values(samplesEpics))

export type RootEpic = typeof rootEpic

export default rootEpic
