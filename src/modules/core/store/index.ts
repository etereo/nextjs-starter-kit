import getConfig from 'next/config'
import { createStore as createStoreBase, applyMiddleware, Store, Middleware } from 'redux'
import { createLogger } from 'redux-logger'
import { createEpicMiddleware } from 'redux-observable'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import { RootAction, RootState, RootService, Reducer } from 'typesafe-actions'
import { persistStore, persistReducer, Persistor, PersistConfig } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import { createWrapper, MakeStore } from 'next-redux-wrapper'

import rootReducer, { initialState } from './root.reducer'
import rootEpic from './root.epics'
import rootService from '../services'
import { isClient } from '../helpers'

export type PersistedStore = Store & { PERSISTOR?: Persistor }

export default function createStore(prevState: RootState = initialState) {
  const epicMiddleware = createEpicMiddleware<RootAction, RootAction, RootState, RootService>({
    dependencies: rootService,
  })

  const {
    publicRuntimeConfig: {
      config: { version, debug },
    },
  } = getConfig()

  const middlewares: Array<Middleware | typeof epicMiddleware> = [epicMiddleware]

  if (debug) {
    // debug redux in node environments, like running tests
    const logger = createLogger({
      colors: {
        title: false,
        prevState: false,
        action: false,
        nextState: false,
        error: false,
      },
    })
    middlewares.push(logger)
  }

  let store: PersistedStore
  if (isClient()) {
    const persistConfig: PersistConfig<RootState, RootState, RootState, RootState> = {
      key: 'root',
      keyPrefix: `${version}_`,
      storage,
      // whitelist: ['auth'],
      stateReconciler: autoMergeLevel2,
    }

    const persistedReducer = persistReducer<RootState, RootAction>(
      persistConfig,
      rootReducer as Reducer<RootState, RootAction>,
    )

    store = createStoreBase(
      persistedReducer,
      prevState,
      composeWithDevTools(applyMiddleware(...middlewares)),
    )
  } else {
    store = createStoreBase(
      rootReducer,
      prevState,
      composeWithDevTools(applyMiddleware(...middlewares)),
    )
  }

  store.PERSISTOR = persistStore(store)

  epicMiddleware.run(rootEpic)

  return store
}

const makeStore: MakeStore<RootState> = () => createStore()

export const wrapper = createWrapper<RootState>(makeStore)
