import * as samplesActions from '../../samples/store/samples.actions'

export default {
  ...samplesActions,
}
