import { StateType, ActionType } from 'typesafe-actions'

// example type-safe store typings
declare module 'typesafe-actions' {
  export type RootState = StateType<typeof import('./root.reducer').default>

  export type RootAction = ActionType<typeof import('./root.actions').default>

  interface Types {
    RootAction: RootAction
  }
}
