import { combineReducers } from 'redux'
import { RootState } from 'typesafe-actions'

import samplesReducer, { sampleInitialState } from '../../samples/store/samples.reducer'

export const initialState: RootState = {
  samples: sampleInitialState,
}

const rootReducer: any = combineReducers({
  samples: samplesReducer,
})

export default rootReducer
