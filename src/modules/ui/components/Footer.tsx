import { Box, Container, makeStyles, Typography } from '@material-ui/core'
import React from 'react'

import { CustomTheme } from '../../theme'

const useStyles = makeStyles(({ breakpoints, palette }: CustomTheme) => ({
  root: {
    backgroundColor: palette.colors?.grey[400],
  },
  copyright: {
    textAlign: 'center',
    [breakpoints.up('lg')]: {
      textAlign: 'right',
    },
  },
}))

export default function Footer() {
  const classes = useStyles()
  return (
    <Container maxWidth={false} className={classes.root} component="footer">
      <Box pt={14} pb={9} px={4}>
        <Typography variant="subtitle1" className={`${classes.copyright} large`}>
          © Etéreo 2020
        </Typography>
      </Box>
    </Container>
  )
}
