import React from 'react'
import { render, cleanup } from '@testing-library/react'

import Footer from '../Footer'

describe('<Footer />', () => {
  afterEach(async () => {
    await cleanup()
  })

  it('renders <Footer /> component', async () => {
    const { container } = render(<Footer />)
    expect(container).toMatchSnapshot()
  })
})
