import React from 'react'
import { CssBaseline, makeStyles } from '@material-ui/core'

import { CustomTheme } from '../../theme'

const useGlobalStyles = makeStyles(({ palette }: CustomTheme) => ({
  '@global': {
    '@import': ['url(https://fonts.googleapis.com/css2?family=Roboto&display=swap)'],
    body: {
      backgroundColor: palette.colors?.grey[300],
    },
    '*': {
      boxSizing: 'border-box',
    },
    a: {
      textDecoration: 'none',
    },
    '.line-through': {
      textDecoration: 'line-through',
    },
    img: {
      maxWidth: '100%',
    },
    '.uppercase': {
      textTransform: 'uppercase',
    },
  },
}))

const GlobalStyles: React.FunctionComponent = () => {
  useGlobalStyles()
  return <CssBaseline />
}

export default GlobalStyles
