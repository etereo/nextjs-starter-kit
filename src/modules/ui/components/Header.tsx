import { Box, makeStyles } from '@material-ui/core'
import React from 'react'

import { CustomTheme } from '../../theme'

import { ReactComponent as Logo } from '../../../../public/static/logo.svg'

const useStyles = makeStyles(({ breakpoints, palette }: CustomTheme) => ({
  root: {
    textAlign: 'left',
    height: '65px',
    lineHeight: '92px',
    backgroundColor: palette.colors?.grey[400],
    [breakpoints.up('lg')]: {
      height: '76px',
      lineHeight: '104px',
    },
  },
  logo: {
    width: '3em',
  },
}))

const Header = () => {
  const classes = useStyles()

  return (
    <Box className={classes.root} component="header">
      <Logo className={classes.logo} />
    </Box>
  )
}

export default Header
