import { Box, makeStyles } from '@material-ui/core'
import React from 'react'
import Header from './Header'
import Footer from './Footer'

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
}))

const Layout: React.SFC = ({ children }) => {
  const classes = useStyles()

  return (
    <Box className={classes.root}>
      <Header />
      <Box flexGrow="1" display="flex" justifyContent="center" alignItems="center">
        {children}
      </Box>
      <Footer />
    </Box>
  )
}

export default Layout
