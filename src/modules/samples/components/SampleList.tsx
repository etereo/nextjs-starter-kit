import React from 'react'
import Link from 'next/link'
import { Typography } from '@material-ui/core'
import { Sample } from '../model'

const SampleList = ({ samples }: { samples: Sample[] }) => {
  return (
    <>
      {samples?.map((sample) => (
        <Link key={sample.sampleId} href="/samples/[sampleId]" as={`/samples/${sample.sampleId}`}>
          <a>
            <Typography variant="body1">{sample.sampleId}</Typography>
          </a>
        </Link>
      ))}
    </>
  )
}

export default SampleList
