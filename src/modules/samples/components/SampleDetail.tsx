import React from 'react'
import Link from 'next/link'
import { Typography, Box } from '@material-ui/core'
import { Sample } from '../model'

const SampleDetail = ({ sample }: { sample?: Sample }) => {
  return (
    <Box>
      <Link href="/samples">
        <a>back</a>
      </Link>
      <Typography variant="body1">{sample ? sample.sampleId : 'NOT FOUND'}</Typography>
    </Box>
  )
}

export default SampleDetail
