/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from 'axios'
import getConfig from 'next/config'

import { mockSamples, mockSample } from './samples.mocks'
import { SampleFilters, Sample } from '../model/samples.model'

const { config } = getConfig().publicRuntimeConfig

const SERVICE_URL = `${config.APIBaseUrl}/samples`

export function getSamples(filters: SampleFilters): Promise<Sample[]> {
  return Promise.resolve(mockSamples)
  // return axios.get(SERVICE_URL).then((response: any) => response.data as Sample)
}

export function getSample(sampleId: string): Promise<Sample> {
  return Promise.resolve(mockSample)
  // return axios.get(`${SERVICE_URL}/${sampleId}`).then((response: any) => response.data as Sample)
}

export default {
  getSamples,
  getSample,
}
