import { Sample } from '../model'

export const mockSample: Sample = {
  sampleId: '1',
  userId: '1',
  name: 'sample name 1',
  description: 'sample description 1',
  date: 1234567,
}

export const mockSamples: Sample[] = Array.from(Array(10)).map((_, i) => ({
  sampleId: `${i}`,
  userId: `${i}`,
  name: `sample name ${i}`,
  description: `sample description ${i}`,
  date: 1234567 + i * 1000,
}))
