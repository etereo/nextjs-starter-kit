import { createAction, createAsyncAction } from 'typesafe-actions'

import { SampleFilters, Sample } from '../model/samples.model'

export const fetchSamplesAsync = createAsyncAction(
  'SAMPLES/FETCH_SAMPLES_REQUEST',
  'SAMPLES/FETCH_SAMPLES_SUCCESS',
  'SAMPLES/FETCH_SAMPLES_FAILURE',
)<SampleFilters, Sample[], Error>()

export const fetchSampleAsync = createAsyncAction(
  'SAMPLES/FETCH_SAMPLE_REQUEST',
  'SAMPLES/FETCH_SAMPLE_SUCCESS',
  'SAMPLES/FETCH_SAMPLE_FAILURE',
)<string, Sample, Error>()

export const setFilter = createAction('SAMPLES/SET_FILTER', (action) => {
  return (filters: SampleFilters) => action(filters)
})

export default {
  setFilter,
  fetchSamplesAsync,
  fetchSampleAsync,
}
