import { createSelector } from 'reselect'
import { RootState } from 'typesafe-actions'

import { SamplesState } from './samples.reducer'

export const selectSamplesState = (state: RootState) => state.samples

export const selectSamples = createSelector(
  selectSamplesState,
  (state: SamplesState) => state.samples,
)
export const selectSample = createSelector(
  selectSamplesState,
  (state: SamplesState) => state.sample,
)

export const selectFilters = createSelector(
  selectSamplesState,
  (state: SamplesState) => state.filters,
)

export default {
  selectSamplesState,
  selectSamples,
  selectSample,
  selectFilters,
}
