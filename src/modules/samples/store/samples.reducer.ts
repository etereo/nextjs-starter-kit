import { createReducer } from 'typesafe-actions'

import { SampleFilters, Sample } from '../model/samples.model'

import { fetchSamplesAsync, fetchSampleAsync, setFilter } from './samples.actions'

export interface SamplesState {
  filters: SampleFilters
  samples: Sample[]
  sample?: Sample
}

export const sampleInitialState: SamplesState = {
  filters: {},
  samples: [],
  sample: undefined,
}

const distributiveReducer = createReducer({} as SamplesState)
  .handleAction(fetchSamplesAsync.success, (state, action) => ({
    ...state,
    samples: [...action.payload],
  }))
  .handleAction(fetchSampleAsync.success, (state, action) => ({
    ...state,
    sample: action.payload,
  }))
  .handleAction(setFilter, (state, action) => ({
    ...state,
    filters: {
      ...action.payload,
    },
  }))

export default distributiveReducer
