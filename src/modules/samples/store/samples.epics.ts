import { RootAction, RootState, RootService, isActionOf } from 'typesafe-actions'
import { Epic } from 'redux-observable'
import { filter, switchMap, map, catchError, withLatestFrom } from 'rxjs/operators'
import { of, from } from 'rxjs'

import { fetchSamplesAsync, fetchSampleAsync } from './samples.actions'

export const getSamplesEpic: Epic<RootAction, RootAction, RootState, RootService> = (
  action$,
  state$,
  { api },
) =>
  action$.pipe(
    filter(isActionOf(fetchSamplesAsync.request)),
    withLatestFrom(state$),
    // filter(([action, state]) => !state.samples[action.payload]),
    switchMap(([action]) =>
      from(api.samples.getSamples(action.payload)).pipe(
        map((payload) => fetchSamplesAsync.success(payload)),
        catchError((error: Error) => of(fetchSamplesAsync.failure(error))),
      ),
    ),
  )

export const getSampleEpic: Epic<RootAction, RootAction, RootState, RootService> = (
  action$,
  state$,
  { api },
) =>
  action$.pipe(
    filter(isActionOf(fetchSampleAsync.request)),
    withLatestFrom(state$),
    // filter(([action, state]) => !state.samples[action.payload]),
    switchMap(([action]) =>
      from(api.samples.getSample(action.payload)).pipe(
        map((payload) => fetchSampleAsync.success(payload)),
        catchError((error: Error) => of(fetchSampleAsync.failure(error))),
      ),
    ),
  )

export default {
  getSamplesEpic,
  getSampleEpic,
}
