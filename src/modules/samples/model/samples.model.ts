export interface Sample {
  sampleId: string
  userId: string
  name: string
  description: string
  date: number
}

export interface SampleFilters {
  userId?: string
  dateStart?: string
  dateEnd?: string
}
