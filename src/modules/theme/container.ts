const containerOverride = {
  MuiContainer: {
    root: {
      paddingLeft: '12px',
      paddingRight: '12px',
    },
  },
}

export default containerOverride
