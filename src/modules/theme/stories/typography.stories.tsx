import { Typography, Box } from '@material-ui/core'
import { storiesOf } from '@storybook/react'
import React from 'react'

storiesOf('UI', module).add('Typography', () => {
  return (
    <Box p={4} bgcolor="white">
      <Typography variant="h1">This is a H1</Typography>
      <Typography variant="h2">This is a H2</Typography>
      <Typography variant="h3">This is a H3</Typography>
      <Typography variant="h4">This is a H4</Typography>
      <Typography variant="h5">This is a H5</Typography>
      <Typography className="semibold" variant="h5">
        This is a H5 semibold
      </Typography>
      <Typography className="bold" variant="h5">
        This is a H5 bold
      </Typography>
      <Typography variant="h6">This is a H6</Typography>
      <Typography className="bold" variant="h6">
        This is a H6 bold
      </Typography>
      <Typography className="large" variant="subtitle1">
        This is a Subtitle1 large
      </Typography>
      <Typography className="large bold" variant="subtitle1">
        This is a Subtitle1 large bold
      </Typography>
      <Typography variant="subtitle1">This is a Subtitle1</Typography>
      <Typography className="bold" variant="subtitle1">
        This is a Subtitle1 bold
      </Typography>
      <Typography className="large" variant="subtitle2">
        This is a Subtitle2 large
      </Typography>
      <Typography className="large semibold" variant="subtitle2">
        This is a Subtitle2 large semibold
      </Typography>
      <Typography className="large bold" variant="subtitle2">
        This is a Subtitle2 large bold
      </Typography>
      <Typography variant="subtitle2">This is a Subtitle2</Typography>
      <Typography className="semibold" variant="subtitle2">
        This is a Subtitle2 semibold
      </Typography>
      <Typography className="bold" variant="subtitle2">
        This is a Subtitle2 bold
      </Typography>
      <Typography>This is a Body1</Typography>
      <Typography className="semibold">This is a Body1 semibold</Typography>
      <Typography className="bold">This is a Body1 bold</Typography>
      <Typography className="grey">This is a Body1 grey</Typography>
      <Typography variant="body2">This is a Body2</Typography>
      <Typography className="semibold" variant="body2">
        This is a Body2 semibold
      </Typography>
      <Typography className="bold" variant="body2">
        This is a Body2 bold
      </Typography>
      <Typography className="grey" variant="body2">
        This is a Body2 grey
      </Typography>
      <Typography className="large" variant="caption">
        This is a Caption Large
      </Typography>
      <br />
      <Typography className="large semibold" variant="caption">
        This is a Caption Large semibold
      </Typography>
      <br />
      <Typography className="large bold" variant="caption">
        This is a Caption Large bold
      </Typography>
      <br />
      <Typography className="bold" variant="caption">
        This is a Caption bold
      </Typography>
      <br />
      <Typography variant="caption">This is a Caption</Typography>
      <br />
      <Typography variant="overline">This is a Overline</Typography>
      <br />
      <Typography variant="button">This is a Button</Typography>
    </Box>
  )
})
