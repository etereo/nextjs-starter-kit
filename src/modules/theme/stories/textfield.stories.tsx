import { Grid, makeStyles, TextField } from '@material-ui/core'
import { text } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import React from 'react'

const useStyles = makeStyles(() => ({
  root: {
    '& button': {
      margin: '15px',
    },
  },
}))

const FormSet = () => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Grid container direction="column">
        <TextField
          id="outlined-name"
          label="Default empty"
          value={text('value', '')}
          margin="normal"
          variant="outlined"
          helperText="Helper text"
        />
      </Grid>
      <Grid container direction="column">
        <TextField
          disabled
          id="outlined-name"
          label="Disabled"
          value=""
          margin="normal"
          variant="outlined"
          helperText="Helper text"
        />
      </Grid>
      <Grid container direction="column">
        <TextField
          disabled
          id="outlined-name"
          label="Disabled"
          value="disabled value"
          margin="normal"
          variant="outlined"
          helperText="Helper text"
        />
      </Grid>
      <Grid container direction="column">
        <TextField
          id="outlined-name"
          label="Field not-validated"
          value="Field validated"
          margin="normal"
          variant="outlined"
          helperText="Helper text"
        />
      </Grid>
      <Grid container direction="column">
        <TextField
          required
          id="outlined-name"
          label="Field validated"
          value="Field validated"
          margin="normal"
          variant="outlined"
          helperText="Helper text"
        />
      </Grid>
      <Grid container direction="column">
        <TextField
          error
          id="outlined-error"
          label="Error empty"
          margin="normal"
          variant="outlined"
          helperText="Helper text"
        />
      </Grid>
      <Grid container direction="column">
        <TextField
          error
          id="outlined-error"
          label="Error filled"
          value="invalid value"
          margin="normal"
          variant="outlined"
          helperText="Helper text"
        />
      </Grid>
    </div>
  )
}

storiesOf('UI', module).add('InputText', () => <FormSet />)
