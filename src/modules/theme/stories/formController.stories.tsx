import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  Typography,
} from '@material-ui/core'
import { storiesOf } from '@storybook/react'
import React from 'react'
import { greyPalette } from '../colors'

storiesOf('UI', module)
  .add('Checkbox', () => {
    return (
      <Grid component={Box} p={2} style={{ backgroundColor: greyPalette[300] }}>
        <FormControlLabel
          control={<Checkbox color="primary" />}
          label="Subscribe me to newsletter"
        />
        <FormControlLabel
          control={<Checkbox color="primary" />}
          label={
            <Typography variant="body1">
              I&aposm agree with
              <Button className="link" component="a" href="https://www.google.es" target="_blank">
                Privacy Policy
              </Button>
            </Typography>
          }
        />
        <FormControlLabel
          control={<Checkbox color="primary" />}
          disabled
          label="Don't subscribe me"
        />
      </Grid>
    )
  })
  .add('Radio button', () => {
    return (
      <Grid my={2} component={Box} style={{ backgroundColor: greyPalette[300] }}>
        <Box p={2}>
          <RadioGroup>
            <FormControlLabel
              control={<Radio color="primary" />}
              value="yes"
              label="Subscribe me to newsletter"
            />
            <FormControlLabel
              control={<Radio color="primary" />}
              value="no"
              label="Don't subscribe me to newsletter"
            />
            <FormControlLabel
              control={<Radio color="primary" />}
              value="puede"
              disabled
              label="May be"
            />
          </RadioGroup>
        </Box>
      </Grid>
    )
  })
