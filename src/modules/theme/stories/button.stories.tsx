import { Box, Button, Grid, makeStyles } from '@material-ui/core'
import { action } from '@storybook/addon-actions'
import { storiesOf } from '@storybook/react'
import React from 'react'
import { bluePalette } from '../colors'

const useStyles = makeStyles(() => ({
  root: {
    '& button': {
      margin: '15px',
    },
  },
}))

const ButtonsSet = () => {
  const classes = useStyles()
  return (
    <>
      <Grid className={classes.root} container direction="column">
        <Grid item xs={6} component={Box} display="flex">
          <Button className="success" variant="contained" onClick={action('clicked primary')}>
            Click on me!
          </Button>
          <Button
            className="success"
            variant="contained"
            disabled
            onClick={action('clicked primary')}
          >
            Click on me!
          </Button>
        </Grid>
        <Grid item xs={6} component={Box} display="flex">
          <Button variant="contained" onClick={action('clicked primary')}>
            Click on me!
          </Button>
          <Button variant="contained" disabled onClick={action('clicked primary')}>
            Click on me!
          </Button>
        </Grid>
        <Grid item xs={6} component={Box} display="flex">
          <Button variant="outlined" onClick={action('clicked primary')}>
            Click on me!
          </Button>
          <Button variant="outlined" disabled onClick={action('clicked primary')}>
            Click on me!
          </Button>
        </Grid>
        <Grid
          item
          xs={6}
          component={Box}
          display="flex"
          style={{ backgroundColor: bluePalette[500] }}
        >
          <Button className="inverse" variant="outlined" onClick={action('clicked primary')}>
            Click on me!
          </Button>
          <Button
            className="inverse"
            variant="outlined"
            disabled
            onClick={action('clicked primary')}
          >
            Click on me!
          </Button>
        </Grid>
        <Grid item xs={6} component={Box} display="flex">
          <Button className="default" onClick={action('clicked primary')}>
            Click on me!
          </Button>
          <Button className="default" disabled onClick={action('clicked primary')}>
            Click on me!
          </Button>
        </Grid>
        <Grid
          item
          xs={6}
          component={Box}
          display="flex"
          style={{ backgroundColor: bluePalette[500] }}
        >
          <Button className="default inverse" onClick={action('clicked primary')}>
            Click on me!
          </Button>
          <Button className="default inverse" disabled onClick={action('clicked primary')}>
            Click on me!
          </Button>
        </Grid>
        <Grid item xs={6} component={Box} display="flex">
          <Button className="link " onClick={action('clicked primary')}>
            Click on me!
          </Button>
          <Button className="link" disabled onClick={action('clicked primary')}>
            Click on me!
          </Button>
        </Grid>
      </Grid>
    </>
  )
}

const ButtonsLinkSet = () => {
  return (
    <>
      <Button
        variant="contained"
        component="a"
        href="https://google.es"
        target="_blank"
        onClick={action('clicked secondary')}
      >
        Click on me!
      </Button>
      <br />
      <Button
        component="a"
        href="https://google.es"
        target="_blank"
        onClick={action('clicked secondary')}
      >
        Click on me!
      </Button>
      <br />
      <Button
        className="underline link"
        component="a"
        href="https://google.es"
        target="_blank"
        onClick={action('clicked secondary')}
      >
        Click on me!
      </Button>
    </>
  )
}

storiesOf('UI', module)
  .add('Button', () => <ButtonsSet />)
  .add('Button Link', () => <ButtonsLinkSet />)
