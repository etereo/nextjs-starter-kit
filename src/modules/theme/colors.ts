import { PaletteOptions, ColorPartial } from '@material-ui/core/styles/createPalette'

export interface ColorsPalette extends PaletteOptions {
  colors?: {
    [key: string]: ColorPartial
  }
}

export const greenPalette = {
  400: '#77d511',
  500: '#5EA610',
}

export const purplePalette = {
  300: '#F7EDF7',
  400: '#9C5099',
  500: '#6A2375',
}

export const yellowPalette = {
  300: '#FFDD00',
  500: '#FFD000',
}

export const pinkPalette = {
  300: '#f73378',
  500: '#f50057',
  700: '#ab003c',
}

export const redPalette = {
  300: 'rgba(213, 60, 29, 0.1)',
  400: '#C95942',
  500: '#D53C1D',
}

export const bluePalette = {
  400: '#4a89e2',
  500: '#1467eb',
  700: '#1769aa',
}

export const greyPalette = {
  200: '#F5F5F5',
  300: '#f6f6f6',
  400: '#e7e7e7',
  500: '#767676',
  600: '#484848',
  700: '#cbcbcb',
}

export const commonPalette = {
  white: '#fff',
  black: '#333',
}
