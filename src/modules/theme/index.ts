import { createMuiTheme, Theme, ThemeOptions } from '@material-ui/core/styles'

import fonts from './fonts'
import {
  redPalette,
  greenPalette,
  bluePalette,
  commonPalette,
  greyPalette,
  pinkPalette,
  ColorsPalette,
} from './colors'

import breakpoints from './breakpoints'

import buttonOverride from './button'
import textfieldOverride from './textfield'
import listOverride from './list'
import checkboxOverride from './checkbox'
import radioOverride from './radio'
import paperOverride from './paper'
import formControlOverride from './formControl'
import containerOverride from './container'

export interface CustomThemeOptions extends ThemeOptions {
  palette: ThemeOptions['palette'] & ColorsPalette
}

const commonTheme: CustomThemeOptions = {
  overrides: {
    ...containerOverride,
    ...listOverride,
    ...textfieldOverride,
    ...buttonOverride,
    ...checkboxOverride,
    ...paperOverride,
    ...radioOverride,
    ...formControlOverride,
  },
  spacing: 4,
  typography: {
    fontFamily: fonts.primary,
    fontSize: 16,
    h1: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.big,
      fontWeight: 600,
      lineHeight: 1.38,
      [breakpoints.up('md')]: {
        fontSize: fonts.size.xhuge,
        lineHeight: 'normal',
      },
    },
    h2: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.big,
      fontWeight: 600,
      lineHeight: 1.38,
      [breakpoints.up('md')]: {
        fontSize: fonts.size.huge,
        lineHeight: 'normal',
      },
    },
    h3: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.xlarge,
      fontWeight: 600,
      lineHeight: 1.17,
      [breakpoints.up('md')]: {
        lineHeight: 'normal',
      },
      '&.bold': {
        fontWeight: 700,
      },
    },
    h4: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.large,
      fontWeight: 500,
      lineHeight: 1.35,
      [breakpoints.up('md')]: {
        lineHeight: 'normal',
      },
    },
    h5: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.small,
      fontWeight: 400,
      lineHeight: 1.5,
      // TODO: encapsulate this
      '&.semibold': {
        fontWeight: 600,
      },
      '&.bold': {
        fontWeight: 700,
      },
      '&.grey': {
        color: greyPalette[400],
      },
    },
    h6: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.tiny,
      fontWeight: 400,
      lineHeight: 1.67,
      '&.bold': {
        fontWeight: 700,
      },
    },
    subtitle1: {
      fontSize: fonts.size.base,
      fontWeight: 400,
      lineHeight: 1.5,
      [breakpoints.up('md')]: {
        fontSize: fonts.size.medium,
        lineHeight: 1.44,
      },
      '&.large': {
        fontSize: fonts.size.large,
        lineHeight: 1.45,
      },
      '&.bold': {
        fontWeight: 700,
      },
    },
    subtitle2: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.base,
      fontWeight: 400,
      lineHeight: 1.5,
      [breakpoints.up('md')]: {
        fontSize: fonts.size.medium,
        lineHeight: 1.44,
      },
      '&.large': {
        fontSize: fonts.size.large,
        lineHeight: 1.35,
      },
      '&.semibold': {
        fontWeight: 600,
      },
      '&.bold': {
        fontWeight: 700,
      },
    },
    body1: {
      fontSize: fonts.size.base,
      fontWeight: 400,
      lineHeight: 1.5,
      '&.semibold': {
        fontWeight: 600,
      },
      '&.bold': {
        fontWeight: 700,
      },
      '&.grey': {
        color: greyPalette[400],
      },
      '&.small': {
        [breakpoints?.down('md')]: {
          fontSize: fonts.size.small,
        },
      },
    },
    body2: {
      fontFamily: fonts.secondary,
      fontSize: fonts.size.base,
      fontWeight: 400,
      lineHeight: 1.5,
      '&.semibold': {
        fontWeight: 600,
      },
      '&.bold': {
        fontWeight: 700,
      },
      '&.grey': {
        color: greyPalette[400],
      },
      '&.small': {
        [breakpoints?.down('md')]: {
          fontSize: fonts.size.small,
        },
      },
    },
    caption: {
      fontSize: fonts.size.tiny,
      fontWeight: 400,
      lineHeight: 1.58,
      '&.large': {
        fontSize: fonts.size.small,
      },
      '&.semibold': {
        fontWeight: 600,
      },
      '&.bold': {
        fontWeight: 700,
      },
      '&.grey': {
        color: greyPalette[400],
      },
    },
    button: {
      fontWeight: 700,
      fontFamily: fonts.secondary,
    },
  },
  palette: {
    type: 'light',
    common: commonPalette,
    colors: {
      yellow: pinkPalette,
      red: redPalette,
      green: greenPalette,
      blue: bluePalette,
      purple: bluePalette,
      grey: greyPalette,
    },
    primary: {
      main: bluePalette[500],
      light: bluePalette[400],
      dark: bluePalette[400],
    },
    secondary: {
      main: pinkPalette[500],
      light: greenPalette[400],
      dark: greenPalette[500],
    },
    error: {
      main: redPalette[500],
      light: redPalette[400],
    },
    grey: greyPalette,
    text: {
      primary: commonPalette.black,
      secondary: commonPalette.white,
    },
    action: {
      disabled: greyPalette[400],
    },
  },
  breakpoints,
}

export interface CustomTheme extends Theme {
  palette: Theme['palette'] & ColorsPalette
}

const theme: CustomTheme = createMuiTheme(commonTheme)

export default theme
