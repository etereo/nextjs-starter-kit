const fonts = {
  primary: 'Roboto',
  secondary: 'Roboto',
  size: {
    xhuge: '36px',
    huge: '30px',
    big: '26px',
    xlarge: '24px',
    large: '20px',
    medium: '18px',
    base: '16px',
    small: '14px',
    tiny: '12px',
  },
}

export default fonts
