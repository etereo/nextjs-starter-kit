import fonts from './fonts'
import { bluePalette, redPalette, greyPalette, greenPalette } from './colors'

const textfieldOverride = {
  MuiTextField: {
    root: {
      '& legend': {
        fontSize: '0.7em',
      },
      '& .MuiFormLabel-asterisk': {
        display: 'none',
      },
      '& label': {
        fontSize: '16px',
        color: '#333',
        '&.Mui-focused': {
          color: bluePalette[500],
        },
        '&.Mui-error:not(.Mui-focused)': {
          color: redPalette[500],
        },
        '& svg': {
          marginLeft: '5px',
          top: '-1px',
          position: 'relative',
        },
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: bluePalette[500],
      },
      '& .MuiOutlinedInput-root': {
        '&.Mui-focused fieldset': {
          borderColor: bluePalette[500],
        },
      },
      '& .MuiFormHelperText-root': {
        color: greyPalette[600],
        fontFamily: fonts.primary,
        '&.Mui-error': {
          color: redPalette[500],
        },
      },
      '& .MuiInputBase-root.Mui-disabled ~ .MuiFormHelperText-root': {
        visibility: 'hidden',
      },
      '& input': {
        fontFamily: fonts.primary,
        fontSize: '16px',
      },
      '& input:valid + fieldset': {
        borderColor: greenPalette[500],
      },
      '& .MuiInputBase-root.Mui-disabled': {
        backgroundColor: greyPalette[200],
      },
    },
  },
}

export default textfieldOverride
