import fonts from './fonts'
import { bluePalette, greenPalette, commonPalette, greyPalette, pinkPalette } from './colors'
import breakpoints from './breakpoints'

const buttonOverride = {
  MuiButton: {
    root: {
      color: bluePalette[500],
      '& span': {
        textTransform: 'uppercase',
        fontSize: '18px',
      },
      '&.no-uppercase': {
        '& span': {
          textTransform: 'none',
        },
      },
      '&.default': {
        '&:hover': {
          backgroundColor: 'transparent',
          opacity: 0.4,
          '& span': {
            color: bluePalette[400],
            backgroundColor: 'transparent',
          },
        },
        '&:hover:active': {
          opacity: 1,
          '& span': {
            backgroundColor: 'transparent',
            color: bluePalette[400],
          },
        },
        '&.inverse': {
          color: pinkPalette[500],
          '&:hover': {
            '& span': {
              color: pinkPalette[300],
            },
          },
          '&:hover:active': {
            '& span': {
              color: commonPalette.white,
            },
          },
          '&:disabled': {
            color: commonPalette.white,
            opacity: 0.4,
          },
        },
      },
      '&.link': {
        backgroundColor: 'transparent',
        display: 'contents',
        padding: 0,
        fontSize: 'inherit',
        '& span': {
          backgroundColor: 'transparent',
          fontFamily: fonts.primary,
          fontSize: 'inherit',
          fontWeight: '700',
          textTransform: 'none',
        },
        '&.large': {
          '& span': {
            fontSize: '20px',
          },
        },
        '&:hover p, &:hover span': {
          backgroundColor: 'transparent',
          opacity: 0.8,
        },
        '&.underline': {
          textDecoration: 'underline',
        },
        '&.semibold': {
          '& span': {
            fontWeight: '600',
          },
        },
      },
    },
    contained: {
      backgroundColor: bluePalette[400],
      color: commonPalette.white,
      borderRadius: '25px',
      padding: '7px 32px',
      transition: 'all 0.2s',
      boxShadow:
        '0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12);',
      '&.success': {
        backgroundColor: greenPalette[500],
        '&:hover': {
          color: commonPalette.white,
          backgroundImage: `radial-gradient(${greenPalette[400]}, ${greenPalette[500]});`,
        },
        '&.disabled': {
          opacity: 0.4,
        },
      },
      '&:hover': {
        backgroundColor: commonPalette.white,
        color: bluePalette[500],
      },
      '&:active': {
        boxShadow:
          '0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0.14), 0px 3px 14px 2px rgba(0,0,0,0.12);',
      },
      '&:disabled': {
        backgroundColor: greyPalette[400],
        color: commonPalette.white,
        opacity: '0.4',
      },
    },
    outlined: {
      backgroundColor: commonPalette.white,
      border: `2px solid ${bluePalette[500]}`,
      color: bluePalette[500],
      padding: '5px 30px',

      '&.responsive': {
        padding: '6px 6px',
        textTransform: 'none',
        '& span': {
          textTransform: 'none',
        },
        [breakpoints.up('md')]: {
          padding: '5px 30px',
        },
      },

      '&.inverse': {
        backgroundColor: 'transparent',
        color: commonPalette.white,
        border: `2px solid ${commonPalette.white}`,
        '&:hover': {
          backgroundColor: 'rgba(255, 255, 255, 0.5)',
        },
        '&:disabled': {
          borderColor: commonPalette.white,
          color: commonPalette.white,
          opacity: 0.4,
        },
      },

      '&:hover': {
        backgroundColor: bluePalette[400],
      },
      '&:disabled': {
        border: `2px solid ${greyPalette[400]}`,
        color: greyPalette[400],
        opacity: 0.4,
      },
    },
  },
}

export default buttonOverride
