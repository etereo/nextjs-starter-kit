const paperOverride = {
  MuiPaper: {
    elevation1: {
      boxShadow: 'none',
    },
  },
}

export default paperOverride
