const formControlOverride = {
  MuiFormControl: {
    root: {
      '& .MuiFormHelperText-root': {
        minHeight: '1.66em',
      },
    },
  },
}

export default formControlOverride
