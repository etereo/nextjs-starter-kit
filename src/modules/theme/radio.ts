import { commonPalette } from './colors'

const radioOverride = {
  MuiRadio: {
    root: {
      color: commonPalette.black,
      margin: '0 6px',
      padding: '0 3px',
      '&:hover': {
        backgroundColor: 'rgba(106, 35, 117, 0.08)',
      },
    },
  },
}

export default radioOverride
