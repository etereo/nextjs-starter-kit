export interface SEOPage {
  path: string
  brand: string
  h1: string
  title: string
  description: string
  canonical: string
  lang: string
  index: boolean
  follow: boolean
  openGraph?: JSON
  twitter?: JSON
  schema?: JSON
}
