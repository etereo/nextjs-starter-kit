import React from 'react'
import Head from 'next/head'

import MetaTagList from './MetaTagList'
import SchemaScript from './SchemaScript'
import { SEOPage } from '../model'

function MetaHeader({ metadata }: { metadata: SEOPage }) {
  if (!metadata) {
    return (
      <Head>
        <title>title</title>
        <meta name="robots" key="robots" content="noindex, nofollow" />
      </Head>
    )
  }

  return (
    <Head>
      <title>{metadata.title}</title>
      <meta
        name="robots"
        key="robots"
        content={`${metadata.index ? 'index' : 'noindex'},${
          metadata.follow ? 'follow' : 'nofollow'
        }`}
      />
      <meta name="description" content={metadata.description} key="description" />
      <link rel="canonical" href={metadata.canonical} key="canonical" />
      {metadata.openGraph && Object.keys(metadata.openGraph).length > 0 && (
        <MetaTagList prefix="og" content={metadata.openGraph} />
      )}
      {metadata.twitter && Object.keys(metadata.twitter).length > 0 && (
        <MetaTagList prefix="twitter" content={metadata.twitter} />
      )}
      {metadata.schema && Object.keys(metadata.schema).length > 0 && (
        <SchemaScript content={metadata.schema} />
      )}
    </Head>
  )
}

export default MetaHeader
