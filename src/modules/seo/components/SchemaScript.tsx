import React from 'react'

interface SchemaProps {
  content: JSON
}

function Schema({ content }: SchemaProps) {
  return (
    <script
      type="application/ld+json"
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: JSON.stringify(content, null, 4),
      }}
    />
  )
}

export default Schema
