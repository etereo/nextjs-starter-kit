import React from 'react'

interface MetaTagListProps {
  prefix: string
  content: JSON
}

function MetaTagList({ prefix, content }: MetaTagListProps) {
  return (
    <>
      {Object.entries(content).map(([key, value]) => (
        <meta property={`${prefix}:${key}`} content={value} key={`${prefix}:${key}`} />
      ))}
    </>
  )
}

export default MetaTagList
