// Transform strings with accents and other decorators to his simple form
// p.e. Japón -> Japon, München -> Munchen
export function normalizeString(str: string) {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}

export default {
  normalizeString,
}
