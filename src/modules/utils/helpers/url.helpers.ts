export function removeUrlParams(url: string) {
  return url.indexOf('?') >= 0 ? url.substring(0, url.indexOf('?')) : url
}

export function removeUrlHash(url: string) {
  return url.indexOf('#') >= 0 ? url.substr(0, url.indexOf('#')) : url
}
