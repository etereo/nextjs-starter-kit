const codesByNumber: { [n: number]: string } = {
  9: 'Tab',
  13: 'Enter',
  37: 'ArrowLeft',
  38: 'ArrowUp',
  39: 'ArrowRight',
  40: 'ArrowDown',
}

const codesByName: { [s: string]: number } = Object.keys(codesByNumber).reduce((obj, key) => {
  const newObj = { ...obj }
  const value = codesByNumber[+key]
  newObj[value] = +key
  return newObj
}, {} as { [s: string]: number })

/**
 * Gets the keycode from a keyName
 * @param {string} keyName - Keyboard key name
 * @returns {int} KeyCode
 */
export function getKeyCode(keyName: string): number {
  return codesByName[keyName]
}

/**
 * Gets the key name from a keycode
 * @param {int} key - Keyboard keycode
 * @returns {string} keyName
 */
export function getKey(keyCode: number): string {
  return codesByNumber[keyCode]
}

export default (search: number | string) => {
  if (typeof search === 'number') {
    return getKey(search)
  }

  if (typeof search === 'string') {
    return getKeyCode(search)
  }

  return undefined
}
