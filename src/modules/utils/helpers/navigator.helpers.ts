export const isWindows =
  globalThis.navigator !== undefined ? navigator.userAgent.indexOf('Win') !== -1 : undefined

export const isMac =
  globalThis.navigator !== undefined ? navigator.userAgent.indexOf('Mac') !== -1 : undefined

export const isLinux =
  globalThis.navigator !== undefined ? navigator.userAgent.indexOf('Linux') !== -1 : undefined

export const isUnix =
  globalThis.navigator !== undefined ? navigator.userAgent.indexOf('X11') !== -1 : undefined
