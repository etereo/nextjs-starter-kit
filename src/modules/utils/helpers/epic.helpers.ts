import { Store, Action } from 'redux'
import { StateObservable, ActionsObservable, Epic } from 'redux-observable'
import { Subject, of } from 'rxjs'
import { toArray } from 'rxjs/operators'

// from https://github.com/zeit/next.js/blob/canary/examples/with-redux-observable/pages/index.js#L12-L17
export default async function executeSyncEpic<RE extends Epic, RS, RA extends Action<any>>(
  store: Store,
  rootEpic: RE,
  rootService: RS,
  action: RA,
) {
  const state$ = new StateObservable(new Subject(), store.getState())

  const initialActions$ = new ActionsObservable(of(action))

  const initialActionsResult = await rootEpic(initialActions$, state$, rootService)
    .pipe(toArray())
    .toPromise()

  initialActionsResult.filter((a: RA) => typeof a !== 'undefined').forEach(store.dispatch)
}
