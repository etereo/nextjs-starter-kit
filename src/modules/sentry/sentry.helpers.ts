import express from 'express'
// NOTE: This require will be replaced with `@sentry/browser`
// client side thanks to the webpack config in next.config.js
import * as Sentry from '@sentry/node'
import * as SentryIntegrations from '@sentry/integrations'

function isClient() {
  return typeof window !== 'undefined'
}

/**
 * Express middleware to serve sourcemaps only for sentry
 * example `app.get(/\.map$/, sourcemapsForSentryOnly(SENTRY_TOKEN))`
 * @param token Sentry token
 */
export const sourcemapsForSentryOnlyHandler = (token: string, environment: string) => (
  req: express.Request,
  res: express.Response,
  nextcb: express.NextFunction,
) => {
  // In production we only want to serve source maps for Sentry
  if (environment === 'production' && token && req.headers['x-sentry-token'] !== token) {
    res.status(401).send('Authentication access token is required to access the source map.')
    return
  }
  nextcb()
}

export default function sentryInit(params: Sentry.NodeOptions) {
  const sentryOptions: Sentry.NodeOptions = {
    ...params,
    maxBreadcrumbs: 50,
    attachStacktrace: true,
  }

  // When we're developing locally
  if (sentryOptions.environment === 'local') {
    // Don't actually send the errors to Sentry
    sentryOptions.beforeSend = () => null

    // Instead, dump the errors to the console
    sentryOptions.integrations = [
      new SentryIntegrations.Debug({
        // Trigger DevTools debugger instead of using console.log
        debugger: false,
      }),
    ]
  }

  Sentry.init(sentryOptions)

  Sentry.configureScope((scope) => {
    scope.setTag('ssr', isClient() ? 'false' : 'true')
  })

  return {
    Sentry,
  }
}
