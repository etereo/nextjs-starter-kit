/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import Document, { DocumentContext, Main, NextScript, Head } from 'next/document'
import getConfig from 'next/config'
import { ServerStyleSheets as MUIServerStyleSheets } from '@material-ui/styles'

type MyDocumentProps = DocumentContext & { GTMCode: string }

export default class MyDocument extends Document<MyDocumentProps> {
  static async getInitialProps(ctx: DocumentContext) {
    const materialSheets = new MUIServerStyleSheets()
    const originalRenderPage = ctx.renderPage
    const { config } = getConfig().publicRuntimeConfig
    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => materialSheets.collect(<App {...props} />),
        })
      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
        styles: (
          <React.Fragment key="styles">
            {initialProps.styles}
            {materialSheets.getStyleElement()}
          </React.Fragment>
        ),
        GTMCode: config.GTMCode,
      }
    } finally {
      // styledSheet.seal()
    }
  }

  render() {
    return (
      <html lang="en">
        <Head />
        <head>
          <link rel="shortcut icon" type="image/x-icon" href="/static/favicon.ico" />
        </head>
        <body>
          <Main />
          <NextScript />
          <script
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `
            <!-- Google Tag Manager -->
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','${this.props.GTMCode}');</script>
            <!-- End Google Tag Manager -->
          `,
            }}
          />
        </body>
      </html>
    )
  }
}
