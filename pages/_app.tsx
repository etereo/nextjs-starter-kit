/* eslint-disable react/jsx-props-no-spreading  */
import React from 'react'
import App from 'next/app'
import getConfig from 'next/config'
import Router from 'next/router'
import { PersistGate } from 'redux-persist/integration/react'
import { ReactReduxContext } from 'react-redux'
import { Store } from 'redux'
import { Persistor } from 'redux-persist'
import { StylesProvider } from '@material-ui/core'

import { AppShell, isClient } from '../src/modules/core'
import { sentryInit } from '../src/modules/sentry'

import theme from '../src/modules/theme'
import { GlobalStyles } from '../src/modules/ui'
import { PersistedStore, wrapper } from '../src/modules/core/store'

const { config } = getConfig().publicRuntimeConfig

sentryInit({
  dsn: config.sentryDsn,
  environment: config.environment,
  release: `${config.name}@${config.version}`,
})

class MyApp extends App<{ store: Store & { PERSISTOR: Persistor } }> {
  componentDidMount() {
    const { document } = globalThis

    const jssStyles = document.querySelector('#jss-server-side')

    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles)
    }

    Router.events.on('routeChangeComplete', () => {
      // TODO: support for hash param navigation
      globalThis.scrollTo(0, 0)
    })
  }

  render() {
    const { Component, pageProps } = this.props
    if (!isClient()) {
      return (
        <StylesProvider>
          <AppShell theme={theme}>
            <GlobalStyles />
            <Component {...pageProps} />
          </AppShell>
        </StylesProvider>
      )
    }

    return (
      <ReactReduxContext.Consumer>
        {({ store }: { store: PersistedStore }) => (
          <PersistGate persistor={store.PERSISTOR as Persistor} loading={null}>
            <StylesProvider>
              <AppShell theme={theme}>
                <GlobalStyles />
                <Component {...pageProps} />
              </AppShell>
            </StylesProvider>
          </PersistGate>
        )}
      </ReactReduxContext.Consumer>
    )
  }
}

export default wrapper.withRedux(MyApp)
