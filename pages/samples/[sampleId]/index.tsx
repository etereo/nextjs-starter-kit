import React from 'react'
import { GetServerSidePropsContext } from 'next'
import { Store } from 'redux'
import { RootAction, RootState } from 'typesafe-actions'

import { executeSyncEpic } from '../../../src/modules/utils'
import rootEpic, { RootEpic } from '../../../src/modules/core/store/root.epics'
import rootService, { RootService } from '../../../src/modules/core/services'
import { selectSample, Sample } from '../../../src/modules/samples'
import { fetchSampleAsync } from '../../../src/modules/samples/store/samples.actions'
import SampleDetail from '../../../src/modules/samples/components/SampleDetail'
import { wrapper } from '../../../src/modules/core/store'
import Layout from '../../../src/modules/ui/components/Layout'

export default function SampleDetailPage({ sample }: { sample: Sample }) {
  return (
    <Layout>
      <SampleDetail sample={sample} />
    </Layout>
  )
}

type SampleDetailPageInitialProps = GetServerSidePropsContext & { store: Store }

export const getServerSideProps = wrapper.getServerSideProps(
  async ({ store, query }: SampleDetailPageInitialProps) => {
    const { sampleId } = query
    await executeSyncEpic<RootEpic, RootService, RootAction>(
      store,
      rootEpic,
      rootService,
      fetchSampleAsync.request(sampleId as string),
    )

    const state: RootState = store.getState()
    const sample = selectSample(state)

    return {
      props: { sample },
    }
  },
)
