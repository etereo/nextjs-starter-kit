import React, { useEffect } from 'react'
import { NextPageContext } from 'next'
import { useSelector, useDispatch } from 'react-redux'
import { Store } from 'redux'

import { SampleFilters, Sample, selectSamples } from '../../src/modules/samples'
import SampleList from '../../src/modules/samples/components/SampleList'
import { fetchSamplesAsync } from '../../src/modules/samples/store/samples.actions'
import Layout from '../../src/modules/ui/components/Layout'

type SampleListPageInitialProps = NextPageContext & { store: Store }

interface SampleListPageProps {
  samples: Sample[]
  filters?: SampleFilters
}

export default function SampleListPage({ filters }: SampleListPageProps) {
  const samples = useSelector(selectSamples)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchSamplesAsync.request(filters || {}))
  }, [])

  return (
    <Layout>
      <SampleList samples={samples} />
    </Layout>
  )
}

export async function getServerSideProps({ query }: SampleListPageInitialProps) {
  return {
    props: {
      filters: query as SampleFilters,
    },
  }
}
