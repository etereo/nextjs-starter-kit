import { Typography } from '@material-ui/core'
import React from 'react'

import Layout from '../src/modules/ui/components/Layout'

const IndexPage = () => {
  return (
    <Layout>
      <Typography variant="h1">HOME</Typography>
    </Layout>
  )
}

export default IndexPage
