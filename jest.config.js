module.exports = {
  displayName: process.env.npm_package_name,
  name: process.env.npm_package_name,
  preset: 'ts-jest/presets/js-with-ts',
  testEnvironment: 'jsdom',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  moduleNameMapper: {
    '\\.svg': '<rootDir>/src/__mocks__/svgrMock.js',
    '\\.ttf': '<rootDir>/src/__mocks__/ttfMock.js',
    '\\.png': '<rootDir>/src/__mocks__/pngMock.js',
  },
  transform: {
    '^.+\\.(j|t)sx?$': 'ts-jest',
  },
  testPathIgnorePatterns: ['./.next/', './node_modules/'],
  watchPathIgnorePatterns: ['./.next/', './node_modules/'],
  rootDir: '.',
  testMatch: [`<rootDir>/src/**/*.spec.(ts|tsx)`],
  globals: {
    'ts-jest': {
      tsConfig: `<rootDir>/tsconfig.jest.json`,
      isolatedModules: true,
    },
  },
}
